<?php
// included config.
require_once ('config.php');

// Autoload classes
function __autoload($class) {
    // convert namespace to full file path
    $class = APP_ROOT . '/src/' . str_replace('\\', '/', $class) . '.php';
    if (file_exists($class)) {
        require_once($class);
    }
}