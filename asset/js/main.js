(function($){
    $().ready(function(){
        $('#container_comments').each(function(){
            var comments = new Comments();
            comments.init();

            $('input[type=file]').on('change', function () {
                var file_name = $(this).val();
                $(this).parents('form').eq(0).find('.file_name').text(file_name);
            });
        });
    });
})(jQuery);