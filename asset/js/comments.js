var Comments = function() {
    this.data = [];
    this.item_etalon = $();
    this.container_items = '';
    this.form_selector = '';
    this.page_id = null;
    this.home_pages_head = '';
};

Comments.prototype.init = function () {
    this._setPageId();
    this._setHtmlEtalon();
    this._setHomePageHead();
    this._setCommentsContainer();
    this._setSendingForm();
    this.getAllItems();
    this._eventSendComment();
};

Comments.prototype.update = function() {
    $(this.container_items).html('');
    this.getAllItems();
};

Comments.prototype._setHomePageHead = function(selector) {
    if ('undefined' === typeof selector)
        selector = '#head_pages_url';

    this.home_pages_head = $(selector).html();
};

Comments.prototype._setHtmlEtalon = function(selector) {
    if ('undefined' === typeof selector)
        selector = '#comment_etalon';

    this.item_etalon = $(selector).html();
};

Comments.prototype._setCommentsContainer = function(selector) {
    if ('undefined' === typeof selector)
        selector = '#container_comments .my_container';

    this.container_items = selector;
};

Comments.prototype._setSendingForm = function(selector) {
    if ('undefined' === typeof selector)
        selector = '#add_comment';

    this.form_selector = selector;
};

Comments.prototype._setPageId = function () {
    this.page_id = $('body').data('pageid');
};

Comments.prototype.getAllItems = function() {    
    var pageId = this.page_id,
        me = this;

    if (pageId) {
        $.ajax({
            method: 'POST',
            url: '/ajax.php',
            data: {
                page_id: pageId,
                action: 'get_comments',
                url: location.pathname
            },
            dataType: 'json'
        }).done(function( msg ) {
            if ( 'true' === msg.status ) {
                console.log(msg);
                me.data = msg.items;
                me.setHtmlAll();
            }
        });
    }
};

Comments.prototype.setHtmlAll = function() {
    var i = this.data.length,
        me = this,
        path = '';

    // Data sorted dy datetime ASC.
    if (i > 0) {
        while (i--) {
            var temp = me.data[i],
                new_item = $( me.item_etalon ).clone();

            if ('' == location.pathname.replace('/', '')
                || 'home' == location.pathname.replace('/', '')
            ) {
                if (path != temp.url) {
                    path = temp.url;
                    me.addHomePageHead(temp);
                }
            }

            $( new_item ).find('.user_name').text(temp.name);
            $( new_item ).find('.user_comment').text(temp.comment);
            $( new_item ).find('.date').text(temp.datetime);
            
            if (temp.upload_folder) {
                $( new_item ).find('.file').show();
                $( new_item ).find('.file a')
                    .attr('href', temp.upload_folder + temp.file_name)
                    .text(temp.file_name);
            }
            
            $( new_item ).appendTo( me.container_items );
        }
    } else {
        return false;
    }
};

Comments.prototype.addHomePageHead = function(data) {
    var me = this,
        new_item = $( me.home_pages_head ).clone();

    console.log(new_item);
    
    $( new_item ).find('a').attr('href', '/' + data.url).text(data.header);
    $( new_item ).appendTo( me.container_items );
};

Comments.prototype._eventSendComment = function() {
    var me = this,
        form = $(me.form_selector);

    $(form).on('submit', function(e) {
        e.preventDefault();
        var nameField = $(this).find('[name=name]'),
            commentField = $(this).find('[name=comment]'),
            fileField = $(this).find('[name=file]'),
            notValidClass = 'not_valid';

        nameField.removeClass(notValidClass);
        commentField.removeClass(notValidClass);

        if (!nameField.val()) {
            nameField.addClass('not_valid');
            alert('Please input your Name');
            return false;
        }

        if (!commentField.val()) {
            commentField.addClass('not_valid');
            alert('Please input your Comment');
            return false;
        }

        $.ajax({
            method: "POST",
            url: "/ajax.php",
            data: {
                page_id: me.page_id,
                action: "write_comment",
                name: nameField.val(),
                comment: commentField.val()
            },
            dataType: 'json'
        }).done(function( msg ) {
            if ( 'true' === msg.status ) {
                console.log(msg);

                if (fileField.val() && 'undefined' != typeof msg.comment_id) {
                    me._uploadFile(msg.comment_id, fileField);
                } else {
                    me.update();
                    me._clearForm();
                }
            }
        });
    });
};

Comments.prototype._uploadFile = function(comment_id, fileField) {
    var $input = fileField,
        fd = new FormData,
        me = this;

    fd.append('file', $input.prop('files')[0]);

    $.ajax({
        url: '/ajax.php?action=upload_file&comment_id=' + comment_id,
        data: fd,
        processData: false,
        contentType: false,
        type: 'POST',
        success: function (msg) {
            me._clearForm();
            me.update();
        }
    });
};

Comments.prototype._clearForm = function () {
    $(this.form_selector).find('[name]').val('');
    $(this.form_selector).find('.file_name').text('');
};