<?php

namespace views;

interface InterfaceView
{
    public function setData($data);
}