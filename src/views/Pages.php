<?php

namespace views;

class Pages extends \views\Base
{
    function __construct()
    {
        parent::__construct('page.twig');
    }

    public function showHomePage() {
        $tpl = $this->_template->loadTemplate('home.twig');
        echo $tpl->render($this->_data);
    }

    public function showPage() {
        $tpl = $this->_template->loadTemplate($this->_fileTemplate);
        echo $tpl->render($this->_data);
    }
    
    public function showPage404() {
        $tpl = $this->_template->loadTemplate('404.twig');
        echo $tpl->render(array());
    }
}