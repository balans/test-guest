<?php

namespace views;

class Base implements \views\InterfaceView
{
    protected $_template = null;
    protected $_fileTemplate = null;
    protected $_data = null;

    function __construct($fileName)
    {
        $this->_setTemplate();
        $this->_setFileTemplate($fileName);
    }

    protected function _setTemplate() {
        $this->_template = \Singleton::initTemplate();
    }

    protected function _setFileTemplate($fileName)
    {
        $this->_fileTemplate = $fileName;
    }

    public function setData($data) {
        $this->_data = $data;
    }
}