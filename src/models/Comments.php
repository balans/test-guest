<?php

namespace models;

class Comments extends \models\AbstractModel
{
    function __construct()
    {
        parent::__construct();
    }

    protected function _setTableName()
    {
        $this->_table_name = 'comments';
    }

    public function getAllItems() {
        $join_table = 'pages';

        $sql = "
            SELECT {$this->_table_name}.*, {$join_table}.url, {$join_table}.header 
            FROM {$this->_table_name}
                  
            JOIN {$join_table}
            ON {$this->_table_name}.page_id = {$join_table}.id
                
            ORDER BY {$join_table}.url ASC, {$this->_table_name}.datetime ASC";

        $stm  = $this->_dbConnect
            ->prepare($sql);

        $stm->execute();
        return $stm->fetchAll();
    }

    public function getItems($page_id) {
        $stm  = $this->_dbConnect
            ->prepare("
                SELECT * 
                FROM {$this->_table_name} 
                WHERE page_id = ? 
                ORDER BY datetime ASC");

        $stm->execute(array($page_id));
        return $stm->fetchAll();
    }

    public function writeComment($page_id, $name, $comment) {
        $sql = "
            INSERT INTO {$this->_table_name} (`name`, `comment`, `page_id`) 
            VALUES (:name, :comment, :page_id);
        ";
        
        $stmt = $this->_dbConnect
            ->prepare($sql);
        $stmt->bindParam(':name', $name);
        $stmt->bindParam(':comment', $comment);
        $stmt->bindParam(':page_id', $page_id);
        $stmt->execute();
        return $this->_dbConnect->lastInsertId();
    }

    public function updateComment($post_id, $newPath) {
        $sql = "
            UPDATE {$this->_table_name} 
            SET `file_name` = :path
            WHERE `id` = :id;
        ";

        $stmt = $this->_dbConnect
            ->prepare($sql);
        $stmt->bindParam(':id', $post_id);
        $stmt->bindParam(':path', $newPath);
        return $stmt->execute();
    }
}