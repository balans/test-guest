<?php

namespace models;

class Pages extends \models\AbstractModel
{
    function __construct()
    {
        parent::__construct();
    }

    protected function _setTableName()
    {
        $this->_table_name = 'pages';
    }

    public function getPage($url) {
        if ('' == $url)
            $url = 'home';

        $stm  = $this->_dbConnect
            ->prepare("SELECT * FROM {$this->_table_name} WHERE url = ?");
        $stm->execute(array($url));
        return $stm->fetchAll();
    }

    public function getMenu() {
        $stm  = $this->_dbConnect
            ->prepare("SELECT url, header FROM {$this->_table_name}");
        $stm->execute();
        return $stm->fetchAll();
    }
}