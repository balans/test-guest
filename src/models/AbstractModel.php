<?php

namespace models;

abstract class AbstractModel
{
    protected $_dbConnect = null;
    protected $_table_name = null;
    function __construct()
    {
        $this->_dbConnect = \Singleton::initDbConnect();
        $this->_setTableName();
    }
    
    abstract protected function _setTableName();
}