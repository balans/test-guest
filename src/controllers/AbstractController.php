<?php

namespace controllers;

abstract class AbstractController
{
    public function __construct()
    {
        $this->_setModel();
        $this->_setView();
    }

    abstract protected function _setModel();
    abstract protected function _setView();
    
    protected function _getPageUrl() {
        $url = $_SERVER['REQUEST_URI'];
        return str_replace('/', '', $url);
    }
}