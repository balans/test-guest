<?php

namespace controllers;

use \models\Comments as Model;

class Comments extends AbstractController
{
    protected $_model = null;
    protected $_view = null;
    protected $_upload_folder = null;

    function __construct() {
        parent::__construct();
        $this->_setUploadFolder();
    }

    protected function _setModel() {
        $this->_model = new Model();
    }

    protected function _setView() {
        return false;
    }

    public function _setUploadFolder($path = '/uploads/') {
        $this->_upload_folder = $path;
    }

    public function getItems() {
        $page_id = (int) $_REQUEST['page_id'];

        $url =  str_replace('/', '', $_REQUEST['url']);

        if ( '' == $url || 'home' == $url ) {
            $items = $this->_model->getAllItems();
        } else {
            $items = $this->_model->getItems($page_id);
        }

        foreach ($items as &$item) {
            $temp_date = new \DateTime($item['datetime']);
            $item['datetime'] = $temp_date->format('d/m/Y H:i');

            if (!empty($item['file_name'])) {
                $item['upload_folder'] = $this->_upload_folder;
            }
        }

        return $items;
    }
    
    public function writeComment() {
        $page_id = (int) $_REQUEST['page_id'];
        $name = $_REQUEST['name'];
        $comment = $_REQUEST['comment'];
        return $this->_model->writeComment($page_id, $name, $comment);
    }

    public function uploadFile() {
        $post_id = (int) $_REQUEST['comment_id'];
        $tempPath = $this->_renameTheFile($_FILES['file']['name']);
        $newPath = APP_ROOT . $this->_upload_folder . $tempPath;

        if ( 0 < $_FILES['file']['error'] ) {
            return false;
        }

        rename($_FILES['file']['tmp_name'], $newPath);
        return $this->_model->updateComment($post_id, $tempPath);
    }

    protected function _renameTheFile($fileName) {
        $newName = time();
        $arr = explode('.', $fileName);
        $file_extension = $arr[count($arr) - 1];

        switch ($file_extension) {
            case 'html':
            case 'xml':
            case 'php':
                $file_extension = 'txt';
                break;
        }

        if (count($arr) > 1)
            $newName .= '.' . $file_extension;
        return $newName;
    }
}