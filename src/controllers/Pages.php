<?php

namespace controllers;

use \models\Pages as Model;
use \views\Pages as View;

class Pages extends AbstractController
{
    protected $_model = null;
    protected $_view = null;

    function __construct()
    {
        parent::__construct();
    }

    protected function _setModel()
    {
        $this->_model = new Model();
    }

    protected function _setView()
    {
        $this->_view = new View();
    }

    public function getThePage() {
        $url = $this->_getPageUrl();
        $data = $this->_model->getPage($url);
        
        if(!empty($data)) {
            $data = $data[0];
            $menu = $this->_model->getMenu();
            $data['menu'] = $menu;
            $this->_view->setData($data);
            
            if ('home' != $data['url']) {
                $this->_view->showPage();
                return true;
            } else {
                $this->_view->showHomePage();
                return true;
            }
        } else {
            $this->_view->setData(array());
            $this->_view->showPage404();
            return true;
        }
    }
}