<?php

class Singleton {
    protected static $_db;
    protected static $_template;
    private function __construct() {}
    private function __clone() {}
    private function __wakeup() {}

    public static function initDbConnect() {
        if ( is_null(self::$_db) ) {
            self::$_db = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASS);
        }
        return self::$_db;
    }

    public static function initTemplate() {
        if ( is_null(self::$_template) ) {
            require_once APP_ROOT . '/vendor/Twig/Autoloader.php';
            Twig_Autoloader::register();
            $loader = new Twig_Loader_Filesystem(APP_ROOT .'/templates');
            self::$_template = new Twig_Environment($loader);
        }
        return self::$_template;
    }
}