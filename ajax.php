<?php

require_once 'app.php';

$response = new stdClass();

switch ($_REQUEST['action']) {
    case 'get_comments':
        $comment = new \controllers\Comments();
        $response->items = $comment->getItems();

        if (!empty($response->items))
            $response->status = 'true';
        break;

    case 'write_comment':
        $comment = new \controllers\Comments();
        $commentId = $comment->writeComment();
        $response->status = $commentId ? 'true' : 'false';
        $response->comment_id = $commentId;
        break;
    
    case 'upload_file':
        $comment = new \controllers\Comments();
        $response->status = $comment->uploadFile();
        break;
    
    default:
        break;
}

if (!isset($response->status))
    $response->status = 'false';

echo json_encode($response);