module.exports = function(grunt) {

    // Initializes the Grunt tasks with the following settings
    grunt.initConfig({
        production_path: 'web/',
        src_path: 'src/',
        build_path: 'front_build/',
        vendor_path: 'vendor/',

        less: {
            frontend: {
                files: {
                    '<%= build_path %>/main.css': 'asset/less/main.less'
                }
            }
        },
        cssmin: {
            frontend: {
                files: {
                    '<%= production_path %>style.min.css': '<%= build_path %>/main.css'
                }
            }
        },
        copy: {
            js: {
                files: {
                    '<%= production_path %>jquery.min.js': '<%= vendor_path %>jquery.min.js',
                    '<%= production_path %>bootstrap.min.js': '<%= vendor_path %>bootstrap/js/bootstrap.min.js'
                }
            },
            css: {
                files: {
                    '<%= production_path %>bootstrap.min.css': '<%= vendor_path %>bootstrap/css/bootstrap.min.css'
                }
            }
        },
        uglify: {
            frontend: {
                files: {
                    '<%= production_path %>script.min.js': ['asset/js/main.js', 'asset/js/comments.js']
                }
            }
        },
        jshint: {
            frontend: {
                src: 'src/js/*.js'
            }
        },
        watch: {
            frontend: {
                options: {
                    interrupt: true
                },
                files: ['asset/js/**', 'asset/less/**'],
                tasks: ['jshint', 'less:frontend', 'cssmin', 'uglify:frontend']
            }
        }
    });

    // Load the plugins that provide the tasks we specified in package.json.
    grunt.loadNpmTasks('grunt-contrib-jshint');
    // grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-copy');

    // This is the default task being executed if Grunt
    // is called without any further parameter.
    grunt.registerTask('default', ['jshint',  'uglify', 'less', 'cssmin', 'copy']);
};
